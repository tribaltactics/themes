<?php

Route::filter('theme', function($route, $request, $name)
{	
	$theme = Config::get('themes::themes.' . $name);

	if ($theme["base"])
		ThemeManager::setBaseTheme($theme["base"]);

	if ($theme["override"])
		ThemeManager::setOverrideTheme($theme["override"]);
});
