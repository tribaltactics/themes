<?php

return array(
	"themesPath" => "public/themes",
	"themes" => array(
		"site" => array(
			"base" => "association/lte",
			"override" => "association/mashiegolfassociation"
		),
		"admin" => array(
			"base" => "default",
			"override" => "mashiegolfassociation"
		),

	)
);