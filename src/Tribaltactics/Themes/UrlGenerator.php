<?php namespace Tribaltactics\Themes;

use Log, ThemeManager, File;

class UrlGenerator extends \Illuminate\Routing\UrlGenerator{
	/**
	 * Generate a URL to an application asset.
	 *
	 * @param  string  $path
	 * @param  bool    $secure
	 * @return string
	 */
	public function asset($path, $secure = null){
		if ($this->isValidUrl($path)) return $path;

		// Get the paths to the theme folders.
		$themePaths = ThemeManager::getThemePaths();
		if (is_array($themePaths)){
			$path = $this->findAssetUrlInPaths($path, $themePaths);
		}

		// Once we get the root URL, we will check to see if it contains an index.php
		// file in the paths. If it does, we will remove it since it is not needed
		// for asset paths, but only for routes to endpoints in the application.
		$root = $this->getRootUrl($this->getScheme($secure));

		return $this->removeIndex($root).'/'.trim($path, '/');
	}

	/**
	 * Find the given asset in the list of paths.
	 *
	 * @param  string  $name
	 * @param  array   $paths
	 * @return string
	 *
	 * @throws \InvalidArgumentException
	 */
	protected function findAssetUrlInPaths($name, $paths)
	{	
		// Make sure the leading directory seperator exists.
		if ($name[0] != "/")
			$name = "/" . $name;

		foreach ((array) $paths as $path)
		{
			if (File::exists($assetPath = $path.$name))
			{
				return $assetUrl = str_replace(public_path(), "", $assetPath);
			}
		}

		throw new \InvalidArgumentException("Asset [$name] not found at: " . $assetPath);
	}
}