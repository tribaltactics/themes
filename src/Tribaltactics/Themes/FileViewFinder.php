<?php namespace Tribaltactics\Themes;

use Log;

/**
 * Extends the core FileViewFinder class
 */
class FileViewFinder extends \Illuminate\View\FileViewFinder{ 

    /**
     * Prepend a location to the finder.
     *
     * @param  string  $location
     * @return void
     */
    public function prependLocation($location)
    {
        if (!in_array($location, $this->paths))
            array_unshift($this->paths, $location);
    }
}
