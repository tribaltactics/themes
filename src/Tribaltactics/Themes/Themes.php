<?php namespace Tribaltactics\Themes;

use Config, Log;

class Themes {
	
	/**
	* @var string baseTheme The base theme to use or override
	*/
	protected $baseTheme;
	
	/**
	* @var string override The override theme used to customize the base theme
	*/
	protected $overrideTheme;

	/**
	* @var class finder The view finder class
	*/
	protected $finder;

	/**
	* @var class resourceFinder The resource finder class. The resource finder is used for including theme specific php resources.
	*/
	protected $resourceFinder;

	/**
	* @var array paths The variable that holds the theme paths.
	*/
	protected $paths = array();

	public function __construct(\Illuminate\View\ViewFinderInterface $finder, ResourceFinderInterface $resourceFinder){
		$this->finder = $finder;
		$this->resourceFinder = $resourceFinder;
	}

	/**
	* Set Base Theme
	* Sets the base theme to the specified theme. This has to be configured in the config file for the package.
	*
	* @param string name The name of the theme. This must be the same as the key in the themes array in the config file.
	*/
	public function setBaseTheme($name){
		$this->baseTheme = $name;
		
		// Add the views folder to the view factory
		$this->finder->prependLocation($this->getThemePath($name) . "/views");

		$this->prependLocation($this->getThemePath($name));
		$this->resourceFinder->prependLocation($this->getThemePath($name));
	}

	/**
	* Set Override Theme
	* Sets the override theme to the specified theme. This has to be configured in the config file for the package.
	*
	* @param string name The name of the override theme. This must be the same as the key in the themes array in the config file.
	*/
	public function setOverrideTheme($name){
		$this->overrideTheme = $name;

		// Add the views folder to the view factory
		$this->finder->prependLocation($this->getThemePath($name) . "/views");

		$this->prependLocation($this->getThemePath($name));		
		$this->resourceFinder->prependLocation($this->getThemePath($name));
	}

	/**
	*
	*
	*
	*/
	public function getThemePath($name){
		$themePath = base_path() . "/" . Config::get("themes::config.themesPath") . "/" . $name;		

		return $themePath;
	}

	public function getThemePaths(){
		return $this->paths;
	}	

	public function getFinder(){
		return $this->finder;
	}

	public function getResourceFinder(){
		return $this->resourceFinder;
	}

	/**
     * Prepend a location to the finder.
     *
     * @param  string  $location
     * @return void
     */
    public function prependLocation($location){
    	if (!in_array($location, $this->paths))
            array_unshift($this->paths, $location);
    }
}