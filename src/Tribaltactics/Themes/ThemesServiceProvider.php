<?php namespace Tribaltactics\Themes;

use Illuminate\Support\ServiceProvider;

class ThemesServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('tribaltactics/themes');

		include __DIR__.'/../../filters.php';
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// override core view finder
		$this->app['view.finder'] = $this->app->share(function($app)
		{
			$paths = $app['config']['view.paths'];

			return new FileViewFinder($app['files'], $paths);
		});

		// Override the core url generator
		$this->app['url'] = $this->app->share(function($app)
		{
			// The URL generator needs the route collection that exists on the router.
			// Keep in mind this is an object, so we're passing by references here
			// and all the registered routes will be available to the generator.
			$routes = $app['router']->getRoutes();

			return new UrlGenerator($routes, $app->rebinding('request', function($app, $request)
			{
				$app['url']->setRequest($request);
			}));
		});

		// create the asset finder
		$this->app['resource.finder'] = $this->app->share(function($app)
		{
			$paths = array(public_path(), base_path());

			return new FileResourceFinder($app['files'], $paths);
		});

		$this->app['themes'] = $this->app->share(function($app)
		{
			$theme = new Themes($app["view.finder"], $app["resource.finder"]);

			//$theme->setBaseTheme('site.default');
			//$theme->setActiveTheme('site.misocial');

			return $theme;
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('themes');
	}

}
